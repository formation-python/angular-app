import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ClassroomService } from '../services/classroom.service';

@Component({
  selector: 'app-classroom-details',
  templateUrl: './classroom-details.component.html',
  styleUrls: ['./classroom-details.component.css']
})
export class ClassroomDetailsComponent implements OnInit {

  name: string = "Salle de classe";
  status: boolean = true;
  studentsNumber: number = 0;
  id: string = '';

  classroom = {
    name: '',
    studentsNum: 0,
    status: false
  };

  constructor(private classroomService: ClassroomService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id']
    this.name = this.classroomService.getClassroomById(+this.id).name;
    this.studentsNumber = this.classroomService.getClassroomById(+this.id).studentsNum;
    this.status = this.classroomService.getClassroomById(+this.id).status;
  }

  getTheClassroomByName(name: String) {
    this.classroom = this.classroomService.getClassroomByName(name);
    this.name = this.classroom.name;
    this.studentsNumber = this.classroom.studentsNum;
    this.status = this.classroom.status;


  }

}
