import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { StudentService } from '../services/student.service';

@Component({
  selector: 'app-new-student',
  templateUrl: './new-student.component.html',
  styleUrls: ['./new-student.component.css']
})
export class NewStudentComponent implements OnInit {

  studentForm!: FormGroup;
  error: boolean = false;

  constructor(private formBuilder: FormBuilder, private studentService: StudentService) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.studentForm = this.formBuilder.group({
      prenom: ['', Validators.required, Validators.minLength(6)],
      familyName: '',
      age: 0,
      presence: true,
      hobbies: this.formBuilder.array([])
    })

  }

  getHobbies(): FormArray {
    return this.studentForm.get('hobbies') as FormArray;

  }

  onAddHobby() {
    const newHobbyControl = this.formBuilder.control(null);
    this.getHobbies().push(newHobbyControl);

  }

  onSubmit() {

    const formValue = this.studentForm.value;
    if (this.studentForm.invalid) {
      this.error = true;

    } else {
      this.error = false;
      console.log(formValue);
      this.studentService.addStudentToList(formValue);
    }

  }

}
