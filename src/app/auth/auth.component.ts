import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { TokenStorageService } from '../services/token-storage.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  authStatus: boolean = false;

  loginSubscription!: Subscription

  loginform: any = {
    username: null,
    password: null
  }

  constructor(private AuthService: AuthService, private router: Router, private storage: TokenStorageService) { }

  ngOnInit(): void {
    this.loginSubscription = this.AuthService.loginSubject.subscribe(
      (_) => {
        this.router.navigate(['/articles'])
      }
    )


    if (this.storage.getToken() != null) {
      this.router.navigate(['/articles'])
    }

  }

  onSingIn() {
    this.AuthService.login(this.loginform)
  }



}
