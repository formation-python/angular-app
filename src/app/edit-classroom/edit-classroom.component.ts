import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ClassroomService } from '../services/classroom.service';

@Component({
  selector: 'app-edit-classroom',
  templateUrl: './edit-classroom.component.html',
  styleUrls: ['./edit-classroom.component.css']
})
export class EditClassroomComponent implements OnInit {

  constructor(private classroomService: ClassroomService) { }

  ngOnInit(): void {
  }

  onSubmit(form: NgForm) {

    const formValue = form.value;
    this.classroomService.addClassroomToList(formValue);
  }

}
