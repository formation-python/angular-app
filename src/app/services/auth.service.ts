import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { TokenStorageService } from './token-storage.service';

const API_AUTH_URL = 'https://django-sample-diginamic.herokuapp.com/auth/';

@Injectable({
  providedIn: 'root'
})


export class AuthService {

  constructor(private httpClient: HttpClient, private storage: TokenStorageService) { }

  loginSubject = new Subject<any>();

  isAuth = true;

  login(loginObject: any) {

    this.httpClient.post<any>(API_AUTH_URL + 'login/', loginObject).subscribe(
      (res) => {
        this.storage.saveToken(res['access'])
        this.emitLoginSubject()
      },
      (error) => {
        console.log(error)
      }
    )

  }

  logout() {
    this.storage.removeToken()

  }

  emitLoginSubject() {
    this.loginSubject.next()
  }


}
