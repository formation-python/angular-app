import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

const API_COMMENTS_URL = 'https://django-sample-diginamic.herokuapp.com/api/articles/';

@Injectable({
  providedIn: 'root'
})
export class RemoteArticlesService {

  constructor(private httpClient: HttpClient) { }


  private articles: any = [];
  articlesSubject = new Subject<any[]>();
  private singleArticle: any;
  singleArticleSubject = new Subject<any>();



  getArticlesFromServer() {
    this.httpClient.get<any[]>(API_COMMENTS_URL).subscribe(
      (response) => {
        console.log(response);
        this.articles = response;
        this.emitArticlesSubject()
      },
      (error) => {
        console.log('Erreur : ' + error.status);

      }
    )
  }

  getArticleFromServerById(id: number) {
    this.httpClient.get<any>(API_COMMENTS_URL + id).subscribe(
      (response) => {
        console.log(response);
        this.singleArticle = response;
        this.emitSingleArticleSubject()
      },
      (error) => {
        console.log('Erreur : ' + error.status);

      }
    )
  }

  addArticle(article: any) {
    this.httpClient.post<any>(API_COMMENTS_URL, article).subscribe(
      (response) => {
        console.log(response);
        this.articles.push(response);
        this.emitArticlesSubject()
      },
      (error) => {
        console.log('Erreur : ' + error.status);

      }
    )
  }

  updateArticle(id: number, article: any) {
    this.httpClient.put<any>(API_COMMENTS_URL + id + '/', article).subscribe(
      (response) => {
        console.log(response);
        this.articles.push(response);
        this.emitArticlesSubject()
      },
      (error) => {
        console.log('Erreur : ' + error.status);

      }
    )
  }

  emitArticlesSubject() {
    this.articlesSubject.next(this.articles.slice())
  }

  emitSingleArticleSubject() {
    this.singleArticleSubject.next(this.singleArticle)
  }

}
