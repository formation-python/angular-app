import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MyFirstComponent } from './my-first/my-first.component';
import { ClassroomComponent } from './classroom/classroom.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSliderModule } from '@angular/material/slider';
import { MatRippleModule } from '@angular/material/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { StudentComponent } from './student/student.component';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { ClassroomService } from './services/classroom.service';
import { NavbarComponent } from './navbar/navbar.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { StudentService } from './services/student.service';
import { AuthComponent } from './auth/auth.component';
import { ClassroomViewComponent } from './classroom-view/classroom-view.component';
import { StudentsViewComponent } from './students-view/students-view.component';
import { AuthService } from './services/auth.service';
import { ClassroomDetailsComponent } from './classroom-details/classroom-details.component';
import { AuthGuardService } from './services/auth-guard.service';
import { StudentDetailsViewComponent } from './student-details-view/student-details-view.component';
import { EditClassroomComponent } from './edit-classroom/edit-classroom.component';
import { NewStudentComponent } from './new-student/new-student.component';
import { HttpClientModule } from '@angular/common/http';
import { ArticlesComponent } from './articles/articles.component';
import { ArticlesViewComponent } from './articles-view/articles-view.component';
import { ArticleDetailViewComponent } from './article-detail-view/article-detail-view.component';
import { authInterceptorProviders } from './helpers/auth.interceptor';
import { TokenStorageService } from './services/token-storage.service';

@NgModule({
  declarations: [
    AppComponent,
    MyFirstComponent,
    ClassroomComponent,
    StudentComponent,
    NavbarComponent,
    AuthComponent,
    ClassroomViewComponent,
    StudentsViewComponent,
    ClassroomDetailsComponent,
    StudentDetailsViewComponent,
    EditClassroomComponent,
    NewStudentComponent,
    ArticlesComponent,
    ArticlesViewComponent,
    ArticleDetailViewComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatSliderModule,
    MatRippleModule,
    // méthode : formulaire par template
    FormsModule,
    //méthode : formulaire réactifs
    ReactiveFormsModule,

    MatListModule,
    MatButtonModule,
    MatCardModule,
    MatExpansionModule,
    MatToolbarModule,
    MatIconModule,
    HttpClientModule
  ],
  providers: [
    authInterceptorProviders,
    ClassroomService,
    StudentService,
    AuthService,
    AuthGuardService,
    TokenStorageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
