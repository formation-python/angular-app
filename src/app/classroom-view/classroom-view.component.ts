import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ClassroomService } from '../services/classroom.service';

@Component({
  selector: 'app-classroom-view',
  templateUrl: './classroom-view.component.html',
  styleUrls: ['./classroom-view.component.css']
})
export class ClassroomViewComponent implements OnInit, OnDestroy {
  classroomList: any = []
  classroomSubscription!: Subscription;


  constructor(private classroomService: ClassroomService) { }
  ngOnDestroy(): void {
    this.classroomSubscription.unsubscribe();
  }

  ngOnInit(): void {
    this.classroomSubscription = this.classroomService.classroomListSubject.subscribe(
      (classes_list: any[]) => {
        this.classroomList = classes_list;
      }
    )
    this.classroomService.emitClassroomSubject()
  }

  disableAllList() {
    this.classroomService.disableAll()
  }

  enableAllList() {
    this.classroomService.enableAll()
  }



}
