import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { RemoteArticlesService } from '../services/remote-articles.service';

@Component({
  selector: 'app-articles-view',
  templateUrl: './articles-view.component.html',
  styleUrls: ['./articles-view.component.css']
})
export class ArticlesViewComponent implements OnInit {
  articlesList: any = []
  articlesSubscription!: Subscription;

  articleform: any = {
    title: null,
    summary: null,
    content: null
  }

  constructor(private articlesService: RemoteArticlesService) { }

  ngOnInit(): void {
    this.articlesSubscription = this.articlesService.articlesSubject.subscribe(
      (articles_list: any[]) => {
        this.articlesList = articles_list;
        console.log('liste des articles');
        console.log(this.articlesList)
        this.articleform = {
          title: null,
          summary: null,
          content: null
        }
      }
    )
    this.articlesService.getArticlesFromServer();

  }

  onArticleSubmit(id?: number) {
    if (id != null) {
      this.articlesService.updateArticle(id, this.articleform);
    } else {
      this.articlesService.addArticle(this.articleform);
    }
  }

}
